# PrivoTrade Demo App#
Web Component demo app built using:

1. [Aurelia](http://www.aurelia.io).
1. [Polymer](http://www.polymer-project.org) and [Kendo UI](http://www.telerik.com/kendo-ui)
1. [TypeScript](http://www.typescriptlang.org/)


### Windows Environment setup ###

1. Install the [Node.js](http://nodejs.org) runtime by downloading the installer. Include **ALL** installation options (default). The [npm](https://www.npmjs.com/) package manager is included.
1. Install [git](http://git-scm.com/download/win), the commandline tool to pull the project from this repository. Select **Use Git from the Windows Command Prompt** in the installer.

1. **Log out and log back in** so that the window environment path variables can apply.

1. From the command line **Start -> run -> "cmd"**, install **globaly** the [jspm](http://jspm.io/) package manager which will be used to install ES6 packages, and [gulp](http://gulpjs.com/), which will serve the files with the commands:

            npm install -g jspm
            npm install -g gulp



### Get the app source code ###

1. From the command line, clone the repo by using the **HTTPS URL from the top right corner of this page**:

         git clone --no-checkout https://<loginname>@bitbucket.org/mrbedo/mrbedo.bitbucket.org.git

1. Navigate into the git repo and configure for long filenames so that windows doesn't complain:

         git config core.longpaths true

1. Check out the files:

         git checkout -f HEAD


### Download Project Dependencies ###
     
1. Download the npm package defitions for the various required build tools by running: (may take a few minutes)
        
        npm install
        
1. Download the jspm package defitions for the various library dependencies:
              
        jspm install

1. Download the bower package defitions for the Polymer dependencies:
              
        bower install
        
### Running the demo app ###

1. Run gulp watch command to serve the files from the project directory:

        gulp watch

1. The page will be served at [http://localhost:9000](http://localhost:9000)


### ES6 Editor ###
To edit the app, consider using a simple text editor for now, such as [Atom](http://atom.io/)
Install the [TypeScript plugin for Atom](http://atom.io/packages/atom-typescript)