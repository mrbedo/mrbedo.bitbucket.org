var gulp = require('gulp');
var browserSync = require('browser-sync');
var runSequence = require('run-sequence');
var changed = require('gulp-changed');
var ts = require('gulp-typescript');
var merge = require('merge2');
var del = require('del');
var vinylPaths = require('vinyl-paths');

var srcDir = {
  ts: "src/**/*.ts",
  dts: "typings/**/*.d.ts",
  html: "src/**/*.html",
  style: "styles/**/*.css",
  scripts: "src/scripts/**/*.js"
}

var outDir = {
  root: "dist",
  scripts: "dist/scripts"
}

gulp.task('clean', function() {
  return gulp.src([outDir.root])
    .pipe(vinylPaths(del));
});

gulp.task('build-ts', function () {
  var tsResult = gulp
  .src([srcDir.ts, srcDir.dts])
  .pipe(
    ts({
       typescript: require('typescript'),
       declarationFiles: false,
       noExternalResolve: true,
       removeComments: true,
       target: "es5",
       module: "amd",
       experimentalDecorators: true
  }));

  return tsResult.js
    //.pipe(changed(outDir.root, {extension: '.js'})) //ends up looking for index.js??
    .pipe(gulp.dest(outDir.root));

  // return merge([
  //     tsResult.dts.pipe(gulp.dest(outDir.root)),
  //     tsResult.js.pipe(gulp.dest(outDir.root))
  // ]);
});

gulp.task('build-html', function () {
  return gulp.src(srcDir.html)
    .pipe(changed(outDir.root))
    .pipe(gulp.dest(outDir.root));
});

gulp.task('build-scripts', function () {
  return gulp.src(srcDir.scripts)
    .pipe(changed(outDir.scripts))
    .pipe(gulp.dest(outDir.scripts));
});

gulp.task('build', function(callback){
  return runSequence(
    'clean',
    'build-html',
    'build-ts',
    'build-scripts',
    callback
  );
});

gulp.task('serve', ['build'], function(done) {
  browserSync({
    open: false,
    port: 9000,
    server: {
      baseDir: ['.'],
      middleware: function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        next();
      }
    }
  }, done);
});

gulp.task('watch', ['serve'], function() {
  gulp.watch(srcDir.ts, ['build-ts', browserSync.reload]).on('change', reportChange);
  gulp.watch(srcDir.dts, ['build-ts', browserSync.reload]).on('change', reportChange);
  gulp.watch(srcDir.html, ['build-html', browserSync.reload]).on('change', reportChange);
  gulp.watch(srcDir.scripts, ['build-scripts', browserSync.reload]).on('change', reportChange);
});

function reportChange(event){
  console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
}
