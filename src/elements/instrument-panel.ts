import {bindable} from 'aurelia-framework';
import * as moment from 'moment';
import {IQuote, PriceFeed} from '../model/pricefeed';

export class InstrumentPanel{
  _quoteTimer: any;
  _tickerTimer: any;
  _element: any;
  _chart: any;
  _feed: PriceFeed;
  _isSubscribed: boolean;

  constructor(element){
    this._element = element;
    this._feed = new PriceFeed();
  }

  @bindable lastUpdated: any;
  @bindable chartData: any;
  @bindable historicalData: IQuote[];
  @bindable historicalSubData: any;
  @bindable instrument: any;

  instrumentChanged(obj){
    console.log("Instrument changed to: ");
    console.dir(obj);
    this.reset();
    this.subscribe(obj);
  }

  @bindable tickerUpdateRate: number = 5; //seconds
  tickerUpdateRateChanged(rate: number){
    if(rate <= 0){
      rate = 1;
    }

    if(this._isSubscribed){
      console.log("ticker item rate changed: " + rate);
      clearInterval(this._tickerTimer);

      this.initSubscriptionData();
      this._tickerTimer = setInterval(() => this.onDataTimerTick(), rate * 1000);
    }
  }

  @bindable quote;
  quoteChanged(value){
    console.log("Got quote: ");
    console.dir(value);
  }

  @bindable selectedItems;
  selectedItemsChanged(item) {
    let selection = item[0];

    if(selection == null){
      //Nothing is selected, force the chart to refresh with live data
      this.onDataTimerTick();
      return;
    }

    let date = selection.Date;
    let min = moment(date);
    let max = moment(date);
    max.add(24, 'h');

    let minDate = min.toDate().getTime();
    let maxDate = max.toDate().getTime();
    this.chartData = this.getPriceRange(minDate, maxDate);
  }

  detached(){
    this.unsubscribe();
    this.reset();
  }

  subscribe(symbol){
    this._isSubscribed = true;
    this.tickerUpdateRateChanged(this.tickerUpdateRate);
  }

  unsubscribe(){
    clearInterval(this._tickerTimer);
    this._isSubscribed = false;
  }

  reset(){
    this.quote = null;
    this.chartData = null;
    this.historicalData = null;
  }

  initSubscriptionData(){
    this.onDataTimerTick();
    this.getHistoricalData();
    // this.getNewsData();
  }

  onDataTimerTick(){
    console.log("refreshing data for instrument:");
    console.dir(this.instrument.symbol);

    this.getQuoteData();

    //Only refresh the datachart when no particular price date is selected
    if(this.selectedItems == null || this.selectedItems.length == 0){
      this.getChartData();
    }

    this.lastUpdated = new Date();
  }

  getQuoteData(){
    this._feed
      .getQuoteAsync(this.instrument.symbol)
      .then(result => this.quote = result);
  }

  // getNewsData(){
  //   this._feed
  //     .getNewsAsync(this.instrument.symbol)
  //     .then(result => this.news = result);
  // }

  getChartData(){
    this._feed
      .getRangeAsync(this.instrument.symbol, '1d')
      .then(result => this.chartData = result);
  }

  getHistoricalData(){
    this._feed
      .getHistoricalAsync(this.instrument.symbol)
      .then(result => {
        for(let r of result){
          r.Month = moment(r.Date).format("MMMM");
          r.Year = moment(r.Date).format("YYYY");
        }
        this.historicalData = result;
      });

    this._feed
      .getRangeAsync(this.instrument.symbol, '10d')
      .then(result => this.historicalSubData = result);
  }

  getPriceRange(minDate: number, maxDate: number) {
    let arr = this.historicalSubData;
    let len   = arr.length;
    let mid   = Math.floor(len/2);
    let up    = -1;
    let down  = len;
    let rrange= [];

    while (up++ < mid && down-- > mid){
      if (arr[up].Timestamp >= maxDate || arr[down].Timestamp <= minDate){
        break;
      }

      if (arr[up].Timestamp >= minDate){
        rrange.push(arr[up]);
      }

      if (arr[down].Timestamp <= maxDate){
        rrange.push(arr[down]);
      }
    }

    return rrange;
  }
}
