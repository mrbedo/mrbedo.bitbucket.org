import {bindable} from 'aurelia-framework';
import {computedFrom} from 'aurelia-framework';
import {Server} from '../model/server';
import {ISymbol} from "../model/symbols";

export class SearchPanel{
  constructor(){}
  //private
  _server: Server = new Server();
  _searchTimer: any = null;

  canClearSearch: boolean = false;

  @bindable isSearching: boolean = false;

  //bindable attributes
  @bindable results: ISymbol[] = [];

  search(text: string){
    if(text == null || text.length == 0){
      this.clearSearch();
      return;
    }

    this.isSearching = true;
    window.clearTimeout(this._searchTimer)

    this._searchTimer = window.setTimeout(() => this.doSearch(text), 250);
  }

  clearSearch(){
    window.clearTimeout(this._searchTimer)
    this.isSearching = false;
    this.searchtext = '';
    this.results = [];
    this.selection = null;

    console.log('search cleared');
  }

  doSearch(text: string) {
    console.log("searching: "+ text);
    this._server
      .findTicker(text)
      .then(r => {
        //If search has been cancelled, ignore the result
        if(!this.isSearching) return;
        this.results = r;
        this.isSearching = false;
      });
  }

  //Doesn't work. TODO: Is it a notification source??
  @computedFrom('results')
  get hasResults(): boolean {
    return this.results.length > 0;
  }

  @computedFrom('selection')
  get hasSelection(): boolean{
    return this.selection != null;
  }

  @bindable searchtext: string = "";
  searchtextChanged(text: string){
    this.search(text);
  }

  @bindable selection: ISymbol;
  selectionChanged(symbol: ISymbol){
    console.log("Selection changed:");
    console.dir(symbol);
  }

  isSearchingChanged(obj){
    console.log("isSearching = " + obj);
  }
}
