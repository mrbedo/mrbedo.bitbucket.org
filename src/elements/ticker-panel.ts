import {bindable} from 'aurelia-framework';

export class TickerPanel{
  // Returns a quote object
  // symbol : AAPL
  // AverageDailyVolume : 54237000
  // Change : -0.505
  // DaysLow : 126.670
  // DaysHigh : 128.200
  // YearLow : 74.921
  // YearHigh : 133.600
  // MarketCapitalization : 740.30B
  // LastTradePriceOnly : 127.095
  // DaysRange : 126.670 - 128.200
  // Name : Apple Inc.
  // Symbol : AAPL
  // Volume : 18169270
  // StockExchange : NMS
  @bindable value;
  isPositive;
  valueChanged(obj){
    console.log("Ticker panel value changed to: ");
    console.dir(obj);
    this.isPositive = obj.Change >= 0;
  }
}
