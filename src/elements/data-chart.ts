import {bindable, inject} from 'aurelia-framework';

import '../scripts/telerik/kendo/kendo.all.min';
// import '../../styles/telerik/kendo/kendo.common.min.css!';
// import '../../styles/telerik/kendo/kendo.default.min.css!';
// import '../../styles/telerik/kendo/kendo.dataviz.min.css!';
// import '../../styles/telerik/kendo/kendo.dataviz.default.min.css!';

import 'jquery';


@inject(Element)
export class DataChart{
  _element = null;

  @bindable autoupdate = true;

  @bindable data = [];
  dataChanged(value){
    console.log("Chart data updated: ");
    console.dir(value);

    let chart = $("#chart").data("kendoStockChart");
    if(chart == undefined) return;

    //Enable transitions only on the first render
    chart.options.transitions = chart.dataSource.data().length == 0;
    //Re-read the data
    chart.dataSource.read();

    this.setChartNavigation(chart);

    chart.refresh();
  }

  setChartNavigation(chart){
    let minRange = this.data[0];
    let maxRange = this.data[this.data.length - 1];
    chart._navigator.options.select.from = minRange;
    chart._navigator.options.select.to = maxRange;
  }

  newsItems = [];

  constructor(element){
    this._element = element;
  }

  attached(){
    this.initChart();
  }

  initChart(){
    let parent = this;
    let chart: any = $("#chart");
    chart.kendoStockChart({
      dataSource: {
        transport: {
          read: function(operation) {
              console.log("Reading chart data: ");
              console.dir(parent.data);
              operation.success(parent.data);
          }
        }
      },
      chartArea: {
        height: 300
      },
      axisDefaults: {
        line: {
          color: "#1a1a1a"
          },
        labels: {
          color: "#a8a8a8"
        },
        minorGridLines: {
          color: "#f0f0f0"
        },
        majorGridLines: {
          color: "#e3e3e3"
        },
        title: {
          color: "#232323"
        }
      },
      dateField: "Timestamp",
      categoryAxis: {
        baseUnit: "minutes",
        baseUnitStep: 1,
        // autoBaseUnitSteps: {
        //     minutes: [1, 2, 3],
        //     hours: [1,2]
        // }//,
        // type: "category",
        labels: {
            format: "htt",
            step: 60
        }
      },
      valueAxes: [{
       	name: "priceAxis"
      },{
        name: "volumeAxis",
        visible: false
      }],
      series: [
        // {
        //   type: "candlestick",
        //   openField: "open",
        //   highField: "high",
        //   lowField: "low",
        //   closeField: "close"
        // },
        {
          type: "area",
          field: "close",
          axis: "priceAxis",
          missingValues: "interpolate"//,
          //categoryField: "Timestamp"
        },{
          type: "column",
          field: "volume",
          axis: "volumeAxis"
        }
      ],
      seriesColors: [
        "#00b0f0",
        "#0070c0",
        "#002060",
        "#678900",
        "#ffb53c",
        "#396000"
      ]
      ,navigator: {
        series: {
          height: 100,
          type: "area",
          field: "close",
          color: "#234333"
        }
      }
    });
  }
}
