import {bindable, inject} from 'aurelia-framework';
import 'jquery';
import '../scripts/telerik/kendo/kendo.all.min';
// import '../../styles/telerik/kendo/kendo.common.min.css!';
// import '../../styles/telerik/kendo/kendo.default.min.css!';
// import '../../styles/telerik/kendo/kendo.dataviz.min.css!';
// import '../../styles/telerik/kendo/kendo.dataviz.default.min.css!';

import * as moment from "moment";

@inject(Element)
export class DataGrid{
  _element = null;
  _vm = this;

  @bindable historicaldata = [];

  @bindable
  selected: any;

  constructor(element){
    this._element = element;
  }

  @bindable data;
  dataChanged(value){
    console.log("DataGrid.data =");
    console.dir(value);

    let grid = $("#grid").data("kendoGrid");
    grid.dataSource.data(value);
    //set grouping
    grid.dataSource.group([
      { field: "Year" },
      { field: "Month" }
    ]);
  }

  attached(){
    this.doGrid();
  }

  doGrid(){
    var _parent = this;

    let grid:any = $("#grid");
    grid.kendoGrid({
        datasource:{
          sort: { field: "date", dir: "desc" },
          schema: {
            model: {
              fields: {
                Date: { type: "date" },
                Open: { type: "number"}
              }
            }
          }
        },
        selectable: "multiple",
        groupable: true,
        filterable: true,
        //  {
        //   mode: "row"
        // },
        resizable: true,
        sortable: {
          mode: "multiple",
          allowUnsort: true
        },
        // "Symbol": "AAPL",
        // "Date": "2015-04-28",
        // "Open": "134.46001",
        // "High": "134.53999",
        // "Low": "129.57001",
        // "Close": "130.56",
        // "Volume": "118580700",
        // "Adj_Close": "130.56"
        columns: [
          { field: "Year"},
          { field: "Month"},
          { field: "Date", filterable: { ui: "datetimepicker"}},
          { field: "Adj_Close", title: "Adj. Close"},
          { field: "Close"},
          { field: "Open"},
          { field: "High"},
          { field: "Low"},
          { field: "Volume"},
          { field: "Symbol"}
        ]
        //,detailTemplate: kendo.template($("#template").html())
        ,detailInit: (e) => {
          _parent.detailInit(e, _parent);
        }
    });

    $("#grid")
      .data("kendoGrid")
      .bind("change", e => {
        let selectedRows = e.sender.select();
        let selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
          let dataItem = e.sender.dataItem(selectedRows[i]);
          selectedDataItems.push(dataItem);
        }

        _parent.selected = selectedDataItems;
      });

  }

  onGridChange(e) {
    this.selected = e.select();
  }

  getPriceRange(minDate, maxDate) {
    let arr = this.historicaldata;
    let len   = arr.length;
    let mid   = Math.floor(len/2);
    let up    = -1;
    let down  = len;
    let rrange= [];

    while (up++ < mid && down-- > mid){
      if (arr[up].Timestamp >= maxDate || arr[down].Timestamp <= minDate){
        break;
      }

      if (arr[up].Timestamp >= minDate){
        rrange.push(arr[up]);
      }

      if (arr[down].Timestamp <= maxDate){
        rrange.push(arr[down]);
      }
    }

    return rrange;
  }

  detailInit(e, parent) {
    let date = e.data.Date;
    let min = moment(date);
    let max = moment(date);
    max.add(24, 'h');

    let minDate = min.toDate().getTime();
    let maxDate = max.toDate().getTime();
    let dataRange = parent.getPriceRange(minDate, maxDate);

    e.detailRow.find("#grid").kendoGrid({
      dataSource: {
          data: dataRange,
          // schema: {
          //   model: {
          //     fields: {
          //       Timestamp: {
          //         type: "date"
          //       }
          //     }
          //   }
          // },
          pageSize: 10
        },
        scrollable: {
          virtual: true
        },
        resizable: true,
        sortable: {
          mode: "multiple",
          allowUnsort: true
        },
        pageable: {
          info: true,
          numeric: false,
          previousNext: false
        }
        ,
        columns: [
          { field: "Timestamp", title: "Time"},
          { field: "close", title: "Price"},
          { field: "volume", title: "Volume"}
        ]
    });
  }
}
