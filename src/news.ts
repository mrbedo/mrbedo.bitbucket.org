
import {bindable} from 'aurelia-framework';
import {PriceFeed} from './model/pricefeed';

export class News{
  _feedTimer
  _feed:PriceFeed = new PriceFeed();
  _feedRefreshInterval = 5 * 60 * 1000;//5 minutes;

  newsitems;
  ticker = 'AAPL';

  @bindable selected;
  selectedChanged(value){
    this.ticker = value.symbol;
    this.newsitems = null;
    this.getNews();
  }

  attached(){
    this.getNews();
    this._feedTimer = setInterval(() => this.getNews(), this._feedRefreshInterval);
  }

  detached(){
    clearInterval(this._feedTimer);
  }

  getNews(){
    console.log("Getting news for: " + this.ticker);
    this._feed
      .getNewsAsync(this.ticker)
      .then(r => {
        this.newsitems = r;
      });
  }
}

// /* demo code
// 1.
// import {bindable} from 'aurelia-framework';
//
// 2.
// @bindable selection;
//
// 3.
// selectionChanged(value){
//   this.ticker = value.symbol;
//   this.newsitems = null;
//   this.getNews();
// }
//
// */
