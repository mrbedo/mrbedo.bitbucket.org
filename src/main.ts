import {Aurelia} from 'aurelia-framework'

export function configure(aurelia:Aurelia) {
  aurelia.use
    .standardConfiguration()
    .developmentLogging();

  console.log("Stuff happened?");
  aurelia.start().then(a => a.setRoot('./dist/app'));
}
