import {Router} from 'aurelia-router';

export class App {
  router:Router;

  configureRouter(config, router:Router){
    config.title = 'PrivoTrade';
    config.map([
      { route: ['','stocks'],  moduleId: './stocks',      nav: true, title: 'Stocks', name: 'stocks' },
      { route: ['news'],  moduleId: './news',      		  nav: true, title:'Research', name: 'news' }
    ]);

    this.router = router;
  }
}
