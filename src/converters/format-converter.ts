import * as moment from 'moment';
import * as numeral from 'numeral';

export class formatValueConverter {
  toView(value, format) {
    if(value instanceof Date){
      return moment(value).format(format);
    }

    return numeral(value).format(format);
  }
}
