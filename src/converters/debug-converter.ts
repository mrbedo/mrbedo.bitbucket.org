export class debugValueConverter {
  toView(value) {
    let type = Object.prototype.toString.call( value );

    console.log("%cBinding %s %o", "color: #990099", type, value);
    return value;
  }
}
