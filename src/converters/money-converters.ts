export class UsdValueConverter {
  toView(value) {
    return "$" + value.toFixed(2);
  }
}
