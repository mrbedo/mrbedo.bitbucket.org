import {bindable, inject} from 'aurelia-framework';
import {Symbols, ISymbol} from './model/symbols';
import {Router} from 'aurelia-router';

export interface IAppQueryString{
  s?:string;
  v?:string;
}

@inject(Router)
export class Stocks {

  @bindable selectedInstrument:ISymbol;
  @bindable searchText:string;

  _router:Router;
  _symbols:Symbols = new Symbols();

  constructor(router:Router){
    this._router = router;
  }

  activate(params:IAppQueryString, routeConfig:any){
    this.searchText = params.s;
    let selectedSymbolName:string = params.v;
    this.selectedInstrument = this._symbols.resolve(selectedSymbolName);
  }

  selectedInstrumentChanged(symbol:ISymbol) {
    this.updateQueryString(this.searchText, symbol);
  }

  searchTextChanged(text:string){
    this.updateQueryString(text, this.selectedInstrument);
  }

  updateQueryString(searchValue:string, selectedSymbol:ISymbol){
    let args:IAppQueryString = new Object();
    args.s = searchValue;
    if(selectedSymbol != null || selectedSymbol != undefined){
      args.v = selectedSymbol.symbol;
    }

    this._router.navigateToRoute('stocks', args, {replace: true});
  }
}
