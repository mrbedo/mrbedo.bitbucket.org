import 'jquery';
import * as moment from 'moment';

export interface IPriceQuote{
  AverageDailyVolume?: number; //54237000
  Change?: number; // -0.505
  DaysLow?: number; //126.670
  DaysHigh?: number; //128.200
  YearLow?: number; //74.921
  YearHigh?: number; //133.600
  MarketCapitalization?: string; //740.30B
  LastTradePriceOnly?: number; //127.095
  DaysRange?: string; //126.670 - 128.200
  Name?: string; //Apple Inc.
  Symbol?: string; //AAPL
  Volume?: number; //18169270
  StockExchange?: string; // NMS
}

export interface IQuote{
    Symbol?: string;
    Date: string;
    Open: number;
    High: number;
    Low: number;
    Close: number;
    Volume: number;
    Adj_Close: number;
    Month: string;
    Year: string;
}

export class PriceFeed {

  // Returns a quote object
  // AverageDailyVolume : 54237000
  // Change : -0.505
  // DaysLow : 126.670
  // DaysHigh : 128.200
  // YearLow : 74.921
  // YearHigh : 133.600
  // MarketCapitalization : 740.30B
  // LastTradePriceOnly : 127.095
  // DaysRange : 126.670 - 128.200
  // Name : Apple Inc.
  // Symbol : AAPL
  // Volume : 18169270
  // StockExchange : NMS
  getQuoteAsync(symbol: string): Promise<IPriceQuote> {
    return new Promise((resolve, reject) => {
      let query = "select * from yahoo.finance.quote where symbol='" + symbol + "'";
      let baseUrl = "http://query.yahooapis.com/v1/public/yql?q=";
      let tail = "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";
      let url = baseUrl + encodeURIComponent(query) + tail;
      // Sample working url:
      // http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quote%20where%20symbol%3D%22AAPL%22&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=

      $.getJSON(url, data => {
        var r = data.query.results;
        if(r == null){
          reject("No results");
        }else{
          resolve(r.quote);
        }
      });
    });
  }

  //Returns an array
  //"quote": [
  //   {
  //     "Symbol": "YHOO",
  //     "Date": "2010-03-10",
  //     "Open": "16.51",
  //     "High": "16.94",
  //     "Low": "16.51",
  //     "Close": "16.79",
  //     "Volume": "33088600",
  //     "Adj_Close": "16.79"
  //   },
  //   ...
  // ]
  getHistoricalAsync(symbol: string, endDate: Date = new Date(), startDate: Date = null): Promise<IQuote[]> {

    if(startDate == null){
      startDate = new Date(endDate.toString());
      startDate.setMonth(startDate.getMonth() - 3);
    }

    let sd = moment(startDate).format("YYYY-MM-DD");
    let ed = moment(endDate).format("YYYY-MM-DD");

    return new Promise<IQuote[]>((resolve, reject) =>{
      //Working: http://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.historicaldata%20where%20symbol%20in%20%28%27YHOO%27%29%20and%20startDate%20=%20%272009-09-11%27%20and%20endDate%20=%20%272010-03-10%27&env=store://datatables.org/alltableswithkeys&format=json
      //YQL console (not working): https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.historicaldata%20where%20symbol%3D%22AAPL%22%20and%20startDate%3D'2015-04-20'%20and%20endDate%3D'2015-04-19'&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=
      let query = "select * from yahoo.finance.historicaldata where symbol='" + symbol + "' and startDate='"+ sd +"' and endDate='" + ed + "'";
      let baseUrl = "http://query.yahooapis.com/v1/public/yql?q=";
      let tail = "&env=store://datatables.org/alltableswithkeys&format=json";

      let url = baseUrl + encodeURIComponent(query) + tail;
      $.ajax({
        type: 'GET',
        url: url,
        dataType: 'jsonp',
        jsonp: 'callback',
        success: data =>{
          resolve(data.query.results.quote);
        }
      });
    })
  }

  //Gets a news RSS feed:
  // title        	The news headline, for example "Yahoo! Announces Quarterly Earnings"
  // link         	The Yahoo! Finance URL for this news item.
  // description  	If given, a short summary of the news item. Many news items include an empty description element.
  // guid         	Unique identifier for the item. For news items the guid is the Yahoo! Finance ID for the listing. The attribute isPermaLink is false.
  // pubDate       	The date and time this news item was posted, in the date format defined by RFC822 Section 5, Mon, 256 Sep 17:25:18 -0700.
  getNewsAsync(symbol: string){
    return new Promise((resolve, reject) =>{
      let url = "http://finance.yahoo.com/rss/headline?s=" + symbol;
      $.ajax({
          url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(url),
          dataType: 'json',
          success: data => {
            let f = data.responseData.feed;
            if (!f || !f.entries) {
              reject("News RSS feed rejected");
              return;
            }

            resolve(f.entries);
          }
        });
    });
  }

  //Gets prices within a range, i.e. 1d, 5d, 6m, 1y, 10y...
  //Working:
  //http://chartapi.finance.yahoo.com/instrument/1.0/AAPL/chartdata;type=quote;range=1d/json
  getRangeAsync(symbol: string, range: string = '5d'){
    return new Promise((resolve, reject) =>{
      let query = symbol + "/chartdata;type=quote;range="+ range + "/json";
      let baseUrl = "http://chartapi.finance.yahoo.com/instrument/1.0/";

      let url = baseUrl + query;

      $.ajax({
        type: 'GET',
        url: url,
        dataType: 'jsonp',
        jsonp: 'callback',
        success: data =>{
          let series = data.series;
          for(let s of series){
            s.Timestamp *= 1000; //Data returns seconds, but we need ms, so we mult by 1000.
          }

          //Pad with a 4pm timestamp at the end if the data hasn't yet reached 4pm.
          let lastTimestamp = series[series.length-1].Timestamp;
          let d = new Date(lastTimestamp);
          if(d.getHours() < 16){
            let p = new Date(lastTimestamp);
            p.setHours(16);
            series.push({Timestamp: p.getTime()});
          }

          resolve(series);
        }
      });
    });
  }
}
