import {ISymbol, Symbols} from "./symbols";

export class Server{
  _data:Symbols = new Symbols();

  findTicker(searchText:string) : Promise<ISymbol[]>{
    return new Promise(resolve => {
      resolve(this.search(searchText));
    });
  }

  search(filter:string) : ISymbol[] {
    var text = this.safeTrim(filter);
    if(text.length == 0){
      return [];
    }

    let regex = new RegExp(text, "i");

    return this._data.symbols
      .filter(i => regex.test(i.symbol)
                || regex.test(i.name)
                || regex.test(i.exch));
  }

  safeTrim(text:string) : string{
    return text != null ? text.trim() : "";
  }
}
