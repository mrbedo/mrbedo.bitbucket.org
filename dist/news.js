var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define(["require", "exports", 'aurelia-framework', './model/pricefeed'], function (require, exports, aurelia_framework_1, pricefeed_1) {
    var News = (function () {
        function News() {
            this._feed = new pricefeed_1.PriceFeed();
            this._feedRefreshInterval = 5 * 60 * 1000; //5 minutes;
            this.ticker = 'AAPL';
        }
        News.prototype.selectedChanged = function (value) {
            this.ticker = value.symbol;
            this.newsitems = null;
            this.getNews();
        };
        News.prototype.attached = function () {
            var _this = this;
            this.getNews();
            this._feedTimer = setInterval(function () { return _this.getNews(); }, this._feedRefreshInterval);
        };
        News.prototype.detached = function () {
            clearInterval(this._feedTimer);
        };
        News.prototype.getNews = function () {
            var _this = this;
            console.log("Getting news for: " + this.ticker);
            this._feed
                .getNewsAsync(this.ticker)
                .then(function (r) {
                _this.newsitems = r;
            });
        };
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], News.prototype, "selected");
        return News;
    })();
    exports.News = News;
});
// /* demo code
// 1.
// import {bindable} from 'aurelia-framework';
//
// 2.
// @bindable selection;
//
// 3.
// selectionChanged(value){
//   this.ticker = value.symbol;
//   this.newsitems = null;
//   this.getNews();
// }
//
// */
