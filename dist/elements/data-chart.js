var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define(["require", "exports", 'aurelia-framework', '../scripts/telerik/kendo/kendo.all.min', 'jquery'], function (require, exports, aurelia_framework_1) {
    var DataChart = (function () {
        function DataChart(element) {
            this._element = null;
            this.autoupdate = true;
            this.data = [];
            this.newsItems = [];
            this._element = element;
        }
        DataChart.prototype.dataChanged = function (value) {
            console.log("Chart data updated: ");
            console.dir(value);
            var chart = $("#chart").data("kendoStockChart");
            if (chart == undefined)
                return;
            //Enable transitions only on the first render
            chart.options.transitions = chart.dataSource.data().length == 0;
            //Re-read the data
            chart.dataSource.read();
            this.setChartNavigation(chart);
            chart.refresh();
        };
        DataChart.prototype.setChartNavigation = function (chart) {
            var minRange = this.data[0];
            var maxRange = this.data[this.data.length - 1];
            chart._navigator.options.select.from = minRange;
            chart._navigator.options.select.to = maxRange;
        };
        DataChart.prototype.attached = function () {
            this.initChart();
        };
        DataChart.prototype.initChart = function () {
            var parent = this;
            var chart = $("#chart");
            chart.kendoStockChart({
                dataSource: {
                    transport: {
                        read: function (operation) {
                            console.log("Reading chart data: ");
                            console.dir(parent.data);
                            operation.success(parent.data);
                        }
                    }
                },
                chartArea: {
                    height: 300
                },
                axisDefaults: {
                    line: {
                        color: "#1a1a1a"
                    },
                    labels: {
                        color: "#a8a8a8"
                    },
                    minorGridLines: {
                        color: "#f0f0f0"
                    },
                    majorGridLines: {
                        color: "#e3e3e3"
                    },
                    title: {
                        color: "#232323"
                    }
                },
                dateField: "Timestamp",
                categoryAxis: {
                    baseUnit: "minutes",
                    baseUnitStep: 1,
                    // autoBaseUnitSteps: {
                    //     minutes: [1, 2, 3],
                    //     hours: [1,2]
                    // }//,
                    // type: "category",
                    labels: {
                        format: "htt",
                        step: 60
                    }
                },
                valueAxes: [{
                        name: "priceAxis"
                    }, {
                        name: "volumeAxis",
                        visible: false
                    }],
                series: [
                    // {
                    //   type: "candlestick",
                    //   openField: "open",
                    //   highField: "high",
                    //   lowField: "low",
                    //   closeField: "close"
                    // },
                    {
                        type: "area",
                        field: "close",
                        axis: "priceAxis",
                        missingValues: "interpolate" //,
                    }, {
                        type: "column",
                        field: "volume",
                        axis: "volumeAxis"
                    }
                ],
                seriesColors: [
                    "#00b0f0",
                    "#0070c0",
                    "#002060",
                    "#678900",
                    "#ffb53c",
                    "#396000"
                ],
                navigator: {
                    series: {
                        height: 100,
                        type: "area",
                        field: "close",
                        color: "#234333"
                    }
                }
            });
        };
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], DataChart.prototype, "autoupdate");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], DataChart.prototype, "data");
        DataChart = __decorate([
            aurelia_framework_1.inject(Element), 
            __metadata('design:paramtypes', [Object])
        ], DataChart);
        return DataChart;
    })();
    exports.DataChart = DataChart;
});
