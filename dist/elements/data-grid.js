var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define(["require", "exports", 'aurelia-framework', "moment", 'jquery', '../scripts/telerik/kendo/kendo.all.min'], function (require, exports, aurelia_framework_1, moment) {
    var DataGrid = (function () {
        function DataGrid(element) {
            this._element = null;
            this._vm = this;
            this.historicaldata = [];
            this._element = element;
        }
        DataGrid.prototype.dataChanged = function (value) {
            console.log("DataGrid.data =");
            console.dir(value);
            var grid = $("#grid").data("kendoGrid");
            grid.dataSource.data(value);
            //set grouping
            grid.dataSource.group([
                { field: "Year" },
                { field: "Month" }
            ]);
        };
        DataGrid.prototype.attached = function () {
            this.doGrid();
        };
        DataGrid.prototype.doGrid = function () {
            var _parent = this;
            var grid = $("#grid");
            grid.kendoGrid({
                datasource: {
                    sort: { field: "date", dir: "desc" },
                    schema: {
                        model: {
                            fields: {
                                Date: { type: "date" },
                                Open: { type: "number" }
                            }
                        }
                    }
                },
                selectable: "multiple",
                groupable: true,
                filterable: true,
                //  {
                //   mode: "row"
                // },
                resizable: true,
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                // "Symbol": "AAPL",
                // "Date": "2015-04-28",
                // "Open": "134.46001",
                // "High": "134.53999",
                // "Low": "129.57001",
                // "Close": "130.56",
                // "Volume": "118580700",
                // "Adj_Close": "130.56"
                columns: [
                    { field: "Year" },
                    { field: "Month" },
                    { field: "Date", filterable: { ui: "datetimepicker" } },
                    { field: "Adj_Close", title: "Adj. Close" },
                    { field: "Close" },
                    { field: "Open" },
                    { field: "High" },
                    { field: "Low" },
                    { field: "Volume" },
                    { field: "Symbol" }
                ],
                detailInit: function (e) {
                    _parent.detailInit(e, _parent);
                }
            });
            $("#grid")
                .data("kendoGrid")
                .bind("change", function (e) {
                var selectedRows = e.sender.select();
                var selectedDataItems = [];
                for (var i = 0; i < selectedRows.length; i++) {
                    var dataItem = e.sender.dataItem(selectedRows[i]);
                    selectedDataItems.push(dataItem);
                }
                _parent.selected = selectedDataItems;
            });
        };
        DataGrid.prototype.onGridChange = function (e) {
            this.selected = e.select();
        };
        DataGrid.prototype.getPriceRange = function (minDate, maxDate) {
            var arr = this.historicaldata;
            var len = arr.length;
            var mid = Math.floor(len / 2);
            var up = -1;
            var down = len;
            var rrange = [];
            while (up++ < mid && down-- > mid) {
                if (arr[up].Timestamp >= maxDate || arr[down].Timestamp <= minDate) {
                    break;
                }
                if (arr[up].Timestamp >= minDate) {
                    rrange.push(arr[up]);
                }
                if (arr[down].Timestamp <= maxDate) {
                    rrange.push(arr[down]);
                }
            }
            return rrange;
        };
        DataGrid.prototype.detailInit = function (e, parent) {
            var date = e.data.Date;
            var min = moment(date);
            var max = moment(date);
            max.add(24, 'h');
            var minDate = min.toDate().getTime();
            var maxDate = max.toDate().getTime();
            var dataRange = parent.getPriceRange(minDate, maxDate);
            e.detailRow.find("#grid").kendoGrid({
                dataSource: {
                    data: dataRange,
                    // schema: {
                    //   model: {
                    //     fields: {
                    //       Timestamp: {
                    //         type: "date"
                    //       }
                    //     }
                    //   }
                    // },
                    pageSize: 10
                },
                scrollable: {
                    virtual: true
                },
                resizable: true,
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                pageable: {
                    info: true,
                    numeric: false,
                    previousNext: false
                },
                columns: [
                    { field: "Timestamp", title: "Time" },
                    { field: "close", title: "Price" },
                    { field: "volume", title: "Volume" }
                ]
            });
        };
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], DataGrid.prototype, "historicaldata");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], DataGrid.prototype, "selected");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], DataGrid.prototype, "data");
        DataGrid = __decorate([
            aurelia_framework_1.inject(Element), 
            __metadata('design:paramtypes', [Object])
        ], DataGrid);
        return DataGrid;
    })();
    exports.DataGrid = DataGrid;
});
