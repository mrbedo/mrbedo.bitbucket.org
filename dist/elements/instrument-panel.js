var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define(["require", "exports", 'aurelia-framework', 'moment', '../model/pricefeed'], function (require, exports, aurelia_framework_1, moment, pricefeed_1) {
    var InstrumentPanel = (function () {
        function InstrumentPanel(element) {
            this.tickerUpdateRate = 5; //seconds
            this._element = element;
            this._feed = new pricefeed_1.PriceFeed();
        }
        InstrumentPanel.prototype.instrumentChanged = function (obj) {
            console.log("Instrument changed to: ");
            console.dir(obj);
            this.reset();
            this.subscribe(obj);
        };
        InstrumentPanel.prototype.tickerUpdateRateChanged = function (rate) {
            var _this = this;
            if (rate <= 0) {
                rate = 1;
            }
            if (this._isSubscribed) {
                console.log("ticker item rate changed: " + rate);
                clearInterval(this._tickerTimer);
                this.initSubscriptionData();
                this._tickerTimer = setInterval(function () { return _this.onDataTimerTick(); }, rate * 1000);
            }
        };
        InstrumentPanel.prototype.quoteChanged = function (value) {
            console.log("Got quote: ");
            console.dir(value);
        };
        InstrumentPanel.prototype.selectedItemsChanged = function (item) {
            var selection = item[0];
            if (selection == null) {
                //Nothing is selected, force the chart to refresh with live data
                this.onDataTimerTick();
                return;
            }
            var date = selection.Date;
            var min = moment(date);
            var max = moment(date);
            max.add(24, 'h');
            var minDate = min.toDate().getTime();
            var maxDate = max.toDate().getTime();
            this.chartData = this.getPriceRange(minDate, maxDate);
        };
        InstrumentPanel.prototype.detached = function () {
            this.unsubscribe();
            this.reset();
        };
        InstrumentPanel.prototype.subscribe = function (symbol) {
            this._isSubscribed = true;
            this.tickerUpdateRateChanged(this.tickerUpdateRate);
        };
        InstrumentPanel.prototype.unsubscribe = function () {
            clearInterval(this._tickerTimer);
            this._isSubscribed = false;
        };
        InstrumentPanel.prototype.reset = function () {
            this.quote = null;
            this.chartData = null;
            this.historicalData = null;
        };
        InstrumentPanel.prototype.initSubscriptionData = function () {
            this.onDataTimerTick();
            this.getHistoricalData();
            // this.getNewsData();
        };
        InstrumentPanel.prototype.onDataTimerTick = function () {
            console.log("refreshing data for instrument:");
            console.dir(this.instrument.symbol);
            this.getQuoteData();
            //Only refresh the datachart when no particular price date is selected
            if (this.selectedItems == null || this.selectedItems.length == 0) {
                this.getChartData();
            }
            this.lastUpdated = new Date();
        };
        InstrumentPanel.prototype.getQuoteData = function () {
            var _this = this;
            this._feed
                .getQuoteAsync(this.instrument.symbol)
                .then(function (result) { return _this.quote = result; });
        };
        // getNewsData(){
        //   this._feed
        //     .getNewsAsync(this.instrument.symbol)
        //     .then(result => this.news = result);
        // }
        InstrumentPanel.prototype.getChartData = function () {
            var _this = this;
            this._feed
                .getRangeAsync(this.instrument.symbol, '1d')
                .then(function (result) { return _this.chartData = result; });
        };
        InstrumentPanel.prototype.getHistoricalData = function () {
            var _this = this;
            this._feed
                .getHistoricalAsync(this.instrument.symbol)
                .then(function (result) {
                for (var _i = 0; _i < result.length; _i++) {
                    var r = result[_i];
                    r.Month = moment(r.Date).format("MMMM");
                    r.Year = moment(r.Date).format("YYYY");
                }
                _this.historicalData = result;
            });
            this._feed
                .getRangeAsync(this.instrument.symbol, '10d')
                .then(function (result) { return _this.historicalSubData = result; });
        };
        InstrumentPanel.prototype.getPriceRange = function (minDate, maxDate) {
            var arr = this.historicalSubData;
            var len = arr.length;
            var mid = Math.floor(len / 2);
            var up = -1;
            var down = len;
            var rrange = [];
            while (up++ < mid && down-- > mid) {
                if (arr[up].Timestamp >= maxDate || arr[down].Timestamp <= minDate) {
                    break;
                }
                if (arr[up].Timestamp >= minDate) {
                    rrange.push(arr[up]);
                }
                if (arr[down].Timestamp <= maxDate) {
                    rrange.push(arr[down]);
                }
            }
            return rrange;
        };
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], InstrumentPanel.prototype, "lastUpdated");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], InstrumentPanel.prototype, "chartData");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Array)
        ], InstrumentPanel.prototype, "historicalData");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], InstrumentPanel.prototype, "historicalSubData");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], InstrumentPanel.prototype, "instrument");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Number)
        ], InstrumentPanel.prototype, "tickerUpdateRate");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], InstrumentPanel.prototype, "quote");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], InstrumentPanel.prototype, "selectedItems");
        return InstrumentPanel;
    })();
    exports.InstrumentPanel = InstrumentPanel;
});
