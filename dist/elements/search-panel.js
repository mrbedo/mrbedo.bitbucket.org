var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define(["require", "exports", 'aurelia-framework', 'aurelia-framework', '../model/server'], function (require, exports, aurelia_framework_1, aurelia_framework_2, server_1) {
    var SearchPanel = (function () {
        function SearchPanel() {
            //private
            this._server = new server_1.Server();
            this._searchTimer = null;
            this.canClearSearch = false;
            this.isSearching = false;
            //bindable attributes
            this.results = [];
            this.searchtext = "";
        }
        SearchPanel.prototype.search = function (text) {
            var _this = this;
            if (text == null || text.length == 0) {
                this.clearSearch();
                return;
            }
            this.isSearching = true;
            window.clearTimeout(this._searchTimer);
            this._searchTimer = window.setTimeout(function () { return _this.doSearch(text); }, 250);
        };
        SearchPanel.prototype.clearSearch = function () {
            window.clearTimeout(this._searchTimer);
            this.isSearching = false;
            this.searchtext = '';
            this.results = [];
            this.selection = null;
            console.log('search cleared');
        };
        SearchPanel.prototype.doSearch = function (text) {
            var _this = this;
            console.log("searching: " + text);
            this._server
                .findTicker(text)
                .then(function (r) {
                //If search has been cancelled, ignore the result
                if (!_this.isSearching)
                    return;
                _this.results = r;
                _this.isSearching = false;
            });
        };
        Object.defineProperty(SearchPanel.prototype, "hasResults", {
            //Doesn't work. TODO: Is it a notification source??
            get: function () {
                return this.results.length > 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SearchPanel.prototype, "hasSelection", {
            get: function () {
                return this.selection != null;
            },
            enumerable: true,
            configurable: true
        });
        SearchPanel.prototype.searchtextChanged = function (text) {
            this.search(text);
        };
        SearchPanel.prototype.selectionChanged = function (symbol) {
            console.log("Selection changed:");
            console.dir(symbol);
        };
        SearchPanel.prototype.isSearchingChanged = function (obj) {
            console.log("isSearching = " + obj);
        };
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Boolean)
        ], SearchPanel.prototype, "isSearching");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Array)
        ], SearchPanel.prototype, "results");
        Object.defineProperty(SearchPanel.prototype, "hasResults",
            __decorate([
                aurelia_framework_2.computedFrom('results'), 
                __metadata('design:type', Boolean)
            ], SearchPanel.prototype, "hasResults", Object.getOwnPropertyDescriptor(SearchPanel.prototype, "hasResults")));
        Object.defineProperty(SearchPanel.prototype, "hasSelection",
            __decorate([
                aurelia_framework_2.computedFrom('selection'), 
                __metadata('design:type', Boolean)
            ], SearchPanel.prototype, "hasSelection", Object.getOwnPropertyDescriptor(SearchPanel.prototype, "hasSelection")));
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', String)
        ], SearchPanel.prototype, "searchtext");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], SearchPanel.prototype, "selection");
        return SearchPanel;
    })();
    exports.SearchPanel = SearchPanel;
});
