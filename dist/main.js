define(["require", "exports"], function (require, exports) {
    function configure(aurelia) {
        aurelia.use
            .standardConfiguration()
            .developmentLogging();
        console.log("Stuff happened?");
        aurelia.start().then(function (a) { return a.setRoot('./dist/app'); });
    }
    exports.configure = configure;
});
