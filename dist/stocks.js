var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define(["require", "exports", 'aurelia-framework', './model/symbols', 'aurelia-router'], function (require, exports, aurelia_framework_1, symbols_1, aurelia_router_1) {
    var Stocks = (function () {
        function Stocks(router) {
            this._symbols = new symbols_1.Symbols();
            this._router = router;
        }
        Stocks.prototype.activate = function (params, routeConfig) {
            this.searchText = params.s;
            var selectedSymbolName = params.v;
            this.selectedInstrument = this._symbols.resolve(selectedSymbolName);
        };
        Stocks.prototype.selectedInstrumentChanged = function (symbol) {
            this.updateQueryString(this.searchText, symbol);
        };
        Stocks.prototype.searchTextChanged = function (text) {
            this.updateQueryString(text, this.selectedInstrument);
        };
        Stocks.prototype.updateQueryString = function (searchValue, selectedSymbol) {
            var args = new Object();
            args.s = searchValue;
            if (selectedSymbol != null || selectedSymbol != undefined) {
                args.v = selectedSymbol.symbol;
            }
            this._router.navigateToRoute('stocks', args, { replace: true });
        };
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], Stocks.prototype, "selectedInstrument");
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', String)
        ], Stocks.prototype, "searchText");
        Stocks = __decorate([
            aurelia_framework_1.autoinject, 
            __metadata('design:paramtypes', [aurelia_router_1.Router])
        ], Stocks);
        return Stocks;
    })();
    exports.Stocks = Stocks;
});
