define(["require", "exports", "./symbols"], function (require, exports, symbols_1) {
    var Server = (function () {
        function Server() {
            this._data = new symbols_1.Symbols();
        }
        Server.prototype.findTicker = function (searchText) {
            var _this = this;
            return new Promise(function (resolve) {
                resolve(_this.search(searchText));
            });
        };
        Server.prototype.search = function (filter) {
            var text = this.safeTrim(filter);
            if (text.length == 0) {
                return [];
            }
            var regex = new RegExp(text, "i");
            return this._data.symbols
                .filter(function (i) { return regex.test(i.symbol)
                || regex.test(i.name)
                || regex.test(i.exch); });
        };
        Server.prototype.safeTrim = function (text) {
            return text != null ? text.trim() : "";
        };
        return Server;
    })();
    exports.Server = Server;
});
