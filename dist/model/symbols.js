define(["require", "exports"], function (require, exports) {
    var Symbols = (function () {
        function Symbols() {
            this.symbols = [
                {
                    "symbol": "A",
                    "name": "Agilent Technologies Inc.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "ADRO",
                    "name": "ADURO BIOTECH, INC.",
                    "exch": "NAS",
                    "type": "S",
                    "exchDisp": "NASDAQ",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "ALSEN.PA",
                    "name": "Sensorion",
                    "exch": "PAR",
                    "type": "S",
                    "exchDisp": "Paris",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "EWA",
                    "name": "iShares MSCI Australia",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "AAPL",
                    "name": "Apple Inc.",
                    "exch": "NMS",
                    "type": "S",
                    "exchDisp": "NASDAQ",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "^DJI",
                    "name": "Dow Jones Industrial Average",
                    "exch": "DJI",
                    "type": "I",
                    "exchDisp": "Dow Jones",
                    "typeDisp": "Index"
                },
                {
                    "symbol": "IGA",
                    "name": "Voya Global Advantage and Premi",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "AOR",
                    "name": "iShares Core Growth Allocation",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "AMZN",
                    "name": "Amazon.com Inc.",
                    "exch": "NMS",
                    "type": "S",
                    "exchDisp": "NASDAQ",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "BAC",
                    "name": "Bank of America Corporation",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "B",
                    "name": "Barnes Group Inc.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "ADRO",
                    "name": "ADURO BIOTECH, INC.",
                    "exch": "NAS",
                    "type": "S",
                    "exchDisp": "NASDAQ",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "BX",
                    "name": "The Blackstone Group L.P.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "IBM",
                    "name": "International Business Machines Corporation",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "BAC",
                    "name": "Bank of America Corporation",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "T",
                    "name": "AT&T, Inc.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "MYI",
                    "name": "Blackrock MuniYield Quality Fun",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "IAI",
                    "name": "iShares US Broker-Dealers",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "IEI",
                    "name": "iShares 3-7 Year Treasury Bond",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "BA",
                    "name": "The Boeing Company",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "C",
                    "name": "Citigroup Inc.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "PRTY",
                    "name": "PARTY CITY HOLDCO INC.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "CDTX",
                    "name": "CIDARA THERAPEUTICS, INC.",
                    "exch": "NAS",
                    "type": "S",
                    "exchDisp": "NASDAQ",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "^IXIC",
                    "name": "NASDAQ Composite",
                    "exch": "NIM",
                    "type": "I",
                    "exchDisp": "NASDAQ GIDS",
                    "typeDisp": "Index"
                },
                {
                    "symbol": "GE",
                    "name": "General Electric Company",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "AOR",
                    "name": "iShares Core Growth Allocation",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "CMU",
                    "name": "MFS Municipal Income Trust Comm",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "DIS",
                    "name": "The Walt Disney Company",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "JPM",
                    "name": "JPMorgan Chase & Co.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "VZ",
                    "name": "Verizon Communications Inc.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "D",
                    "name": "Dominion Resources, Inc.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "^DJI",
                    "name": "Dow Jones Industrial Average",
                    "exch": "DJI",
                    "type": "I",
                    "exchDisp": "Dow Jones",
                    "typeDisp": "Index"
                },
                {
                    "symbol": "PWC",
                    "name": "PowerShares Dynamic Market ETF",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "DIS",
                    "name": "The Walt Disney Company",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "HD",
                    "name": "The Home Depot, Inc.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "DD",
                    "name": "E. I. du Pont de Nemours and Company",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "DLTR",
                    "name": "Dollar Tree, Inc.",
                    "exch": "NMS",
                    "type": "S",
                    "exchDisp": "NASDAQ",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "DOW",
                    "name": "The Dow Chemical Company",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "DAL",
                    "name": "Delta Air Lines, Inc.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "NUGT",
                    "name": "Direxion Daily Gold Miners Bull 3X ETF",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "E",
                    "name": "Eni SpA",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "ETSY",
                    "name": "ETSY INC",
                    "exch": "NAS",
                    "type": "S",
                    "exchDisp": "NASDAQ",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "EWA",
                    "name": "iShares MSCI Australia",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "GE",
                    "name": "General Electric Company",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "PWC",
                    "name": "PowerShares Dynamic Market ETF",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "XOM",
                    "name": "Exxon Mobil Corporation",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "SPY",
                    "name": "SPDR S&P 500 ETF",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "AXP",
                    "name": "American Express Company",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "USO",
                    "name": "United States Oil ETF",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "UGA",
                    "name": "United States Gasoline ETF",
                    "exch": "PCX",
                    "type": "E",
                    "exchDisp": "NYSEArca",
                    "typeDisp": "ETF"
                },
                {
                    "symbol": "F",
                    "name": "Ford Motor Co.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "VIRT",
                    "name": "VIRTU FINANCIAL, INC.",
                    "exch": "NAS",
                    "type": "S",
                    "exchDisp": "NASDAQ",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "FNMA",
                    "name": "Federal National Mortgage Association",
                    "exch": "OBB",
                    "type": "S",
                    "exchDisp": "OTC BB",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "FB",
                    "name": "Facebook, Inc.",
                    "exch": "NMS",
                    "type": "S",
                    "exchDisp": "NASDAQ",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "MYI",
                    "name": "Blackrock MuniYield Quality Fun",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "WFC",
                    "name": "Wells Fargo & Company",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "FCX",
                    "name": "Freeport-McMoRan Inc.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "^FTSE",
                    "name": "FTSE 100",
                    "exch": "FSI",
                    "type": "I",
                    "exchDisp": "London",
                    "typeDisp": "Index"
                },
                {
                    "symbol": "FRO",
                    "name": "Frontline Ltd.",
                    "exch": "NYQ",
                    "type": "S",
                    "exchDisp": "NYSE",
                    "typeDisp": "Equity"
                },
                {
                    "symbol": "WFM",
                    "name": "Whole Foods Market, Inc.",
                    "exch": "NMS",
                    "type": "S",
                    "exchDisp": "NASDAQ",
                    "typeDisp": "Equity"
                }
            ];
        }
        Symbols.prototype.resolve = function (symbol) {
            return this.symbols.find(function (s) { return s.symbol === symbol; });
        };
        return Symbols;
    })();
    exports.Symbols = Symbols;
});
