define(["require", "exports", 'moment', 'jquery'], function (require, exports, moment) {
    var PriceFeed = (function () {
        function PriceFeed() {
        }
        // Returns a quote object
        // AverageDailyVolume : 54237000
        // Change : -0.505
        // DaysLow : 126.670
        // DaysHigh : 128.200
        // YearLow : 74.921
        // YearHigh : 133.600
        // MarketCapitalization : 740.30B
        // LastTradePriceOnly : 127.095
        // DaysRange : 126.670 - 128.200
        // Name : Apple Inc.
        // Symbol : AAPL
        // Volume : 18169270
        // StockExchange : NMS
        PriceFeed.prototype.getQuoteAsync = function (symbol) {
            return new Promise(function (resolve, reject) {
                var query = "select * from yahoo.finance.quote where symbol='" + symbol + "'";
                var baseUrl = "http://query.yahooapis.com/v1/public/yql?q=";
                var tail = "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";
                var url = baseUrl + encodeURIComponent(query) + tail;
                // Sample working url:
                // http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quote%20where%20symbol%3D%22AAPL%22&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=
                $.getJSON(url, function (data) {
                    var r = data.query.results;
                    if (r == null) {
                        reject("No results");
                    }
                    else {
                        resolve(r.quote);
                    }
                });
            });
        };
        //Returns an array
        //"quote": [
        //   {
        //     "Symbol": "YHOO",
        //     "Date": "2010-03-10",
        //     "Open": "16.51",
        //     "High": "16.94",
        //     "Low": "16.51",
        //     "Close": "16.79",
        //     "Volume": "33088600",
        //     "Adj_Close": "16.79"
        //   },
        //   ...
        // ]
        PriceFeed.prototype.getHistoricalAsync = function (symbol, endDate, startDate) {
            if (endDate === void 0) { endDate = new Date(); }
            if (startDate === void 0) { startDate = null; }
            if (startDate == null) {
                startDate = new Date(endDate.toString());
                startDate.setMonth(startDate.getMonth() - 3);
            }
            var sd = moment(startDate).format("YYYY-MM-DD");
            var ed = moment(endDate).format("YYYY-MM-DD");
            return new Promise(function (resolve, reject) {
                //Working: http://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.historicaldata%20where%20symbol%20in%20%28%27YHOO%27%29%20and%20startDate%20=%20%272009-09-11%27%20and%20endDate%20=%20%272010-03-10%27&env=store://datatables.org/alltableswithkeys&format=json
                //YQL console (not working): https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.historicaldata%20where%20symbol%3D%22AAPL%22%20and%20startDate%3D'2015-04-20'%20and%20endDate%3D'2015-04-19'&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=
                var query = "select * from yahoo.finance.historicaldata where symbol='" + symbol + "' and startDate='" + sd + "' and endDate='" + ed + "'";
                var baseUrl = "http://query.yahooapis.com/v1/public/yql?q=";
                var tail = "&env=store://datatables.org/alltableswithkeys&format=json";
                var url = baseUrl + encodeURIComponent(query) + tail;
                $.ajax({
                    type: 'GET',
                    url: url,
                    dataType: 'jsonp',
                    jsonp: 'callback',
                    success: function (data) {
                        resolve(data.query.results.quote);
                    }
                });
            });
        };
        //Gets a news RSS feed:
        // title        	The news headline, for example "Yahoo! Announces Quarterly Earnings"
        // link         	The Yahoo! Finance URL for this news item.
        // description  	If given, a short summary of the news item. Many news items include an empty description element.
        // guid         	Unique identifier for the item. For news items the guid is the Yahoo! Finance ID for the listing. The attribute isPermaLink is false.
        // pubDate       	The date and time this news item was posted, in the date format defined by RFC822 Section 5, Mon, 256 Sep 17:25:18 -0700.
        PriceFeed.prototype.getNewsAsync = function (symbol) {
            return new Promise(function (resolve, reject) {
                var url = "http://finance.yahoo.com/rss/headline?s=" + symbol;
                $.ajax({
                    url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(url),
                    dataType: 'json',
                    success: function (data) {
                        var f = data.responseData.feed;
                        if (!f || !f.entries) {
                            reject("News RSS feed rejected");
                            return;
                        }
                        resolve(f.entries);
                    }
                });
            });
        };
        //Gets prices within a range, i.e. 1d, 5d, 6m, 1y, 10y...
        //Working:
        //http://chartapi.finance.yahoo.com/instrument/1.0/AAPL/chartdata;type=quote;range=1d/json
        PriceFeed.prototype.getRangeAsync = function (symbol, range) {
            if (range === void 0) { range = '5d'; }
            return new Promise(function (resolve, reject) {
                var query = symbol + "/chartdata;type=quote;range=" + range + "/json";
                var baseUrl = "http://chartapi.finance.yahoo.com/instrument/1.0/";
                var url = baseUrl + query;
                $.ajax({
                    type: 'GET',
                    url: url,
                    dataType: 'jsonp',
                    jsonp: 'callback',
                    success: function (data) {
                        var series = data.series;
                        for (var _i = 0; _i < series.length; _i++) {
                            var s = series[_i];
                            s.Timestamp *= 1000; //Data returns seconds, but we need ms, so we mult by 1000.
                        }
                        //Pad with a 4pm timestamp at the end if the data hasn't yet reached 4pm.
                        var lastTimestamp = series[series.length - 1].Timestamp;
                        var d = new Date(lastTimestamp);
                        if (d.getHours() < 16) {
                            var p = new Date(lastTimestamp);
                            p.setHours(16);
                            series.push({ Timestamp: p.getTime() });
                        }
                        resolve(series);
                    }
                });
            });
        };
        return PriceFeed;
    })();
    exports.PriceFeed = PriceFeed;
});
