define(["require", "exports"], function (require, exports) {
    var debugValueConverter = (function () {
        function debugValueConverter() {
        }
        debugValueConverter.prototype.toView = function (value) {
            var type = Object.prototype.toString.call(value);
            console.log("%cBinding %s %o", "color: #990099", type, value);
            return value;
        };
        return debugValueConverter;
    })();
    exports.debugValueConverter = debugValueConverter;
});
