define(["require", "exports"], function (require, exports) {
    var UsdValueConverter = (function () {
        function UsdValueConverter() {
        }
        UsdValueConverter.prototype.toView = function (value) {
            return "$" + value.toFixed(2);
        };
        return UsdValueConverter;
    })();
    exports.UsdValueConverter = UsdValueConverter;
});
