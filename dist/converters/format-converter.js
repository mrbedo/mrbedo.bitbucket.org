define(["require", "exports", 'moment', 'numeral'], function (require, exports, moment, numeral) {
    var formatValueConverter = (function () {
        function formatValueConverter() {
        }
        formatValueConverter.prototype.toView = function (value, format) {
            if (value instanceof Date) {
                return moment(value).format(format);
            }
            return numeral(value).format(format);
        };
        return formatValueConverter;
    })();
    exports.formatValueConverter = formatValueConverter;
});
