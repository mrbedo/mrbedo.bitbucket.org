define(["require", "exports"], function (require, exports) {
    var App = (function () {
        function App() {
        }
        App.prototype.configureRouter = function (config, router) {
            config.title = 'PrivoTrade';
            config.map([
                { route: ['', 'stocks'], moduleId: './stocks', nav: true, title: 'Stocks', name: 'stocks' },
                { route: ['news'], moduleId: './news', nav: true, title: 'Research', name: 'news' }
            ]);
            this.router = router;
        };
        return App;
    })();
    exports.App = App;
});
